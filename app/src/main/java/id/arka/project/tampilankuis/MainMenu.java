package id.arka.project.tampilankuis;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import java.util.ArrayList;

public class MainMenu extends AppCompatActivity {

    Button btTradisional,btMancanegara,btIndonesia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_menu);
        ArrayList<menuItem> menu = new ArrayList<menuItem>();

        menu.add(new menuItem("Lagu Tradisional",R.drawable.logomusic));
        menu.add(new menuItem("Lagu Indonesia",R.drawable.logomusic));
        menu.add(new menuItem("Lagu Mancanegara",R.drawable.logomusic));
        menu.add(new menuItem("Set Profil Pemain",R.drawable.user));

        RecyclerView menuRecycle = (RecyclerView) findViewById(R.id.recyclerview);
        CustomAdapter adapterMenu = new CustomAdapter(this, menu);
        menuRecycle.setAdapter(adapterMenu);
        menuRecycle.setLayoutManager(new LinearLayoutManager(this));
    }
}
