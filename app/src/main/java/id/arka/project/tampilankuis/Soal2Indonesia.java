package id.arka.project.tampilankuis;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


public class Soal2Indonesia extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public Soal2Indonesia() {
        // Required empty public constructor
    }

/*------------------------------------------------------------------------*/

    Button btA, btB, btC, btD;
    TextView txtTimer;
    Thread timerThread;

    @Override
    public void onActivityCreated(Bundle savedState) {
        super.onActivityCreated(savedState);
        btA = getActivity().findViewById(R.id.pilihA);
        btB = getActivity().findViewById(R.id.pilihB);
        btC = getActivity().findViewById(R.id.pilihC);
        btD = getActivity().findViewById(R.id.pilihD);
        txtTimer = getActivity().findViewById(R.id.countDown);

        View.OnClickListener benar = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Kuis k = new Kuis();
                int skoring = k.getSkor() + 25;
                k.setSkor(skoring);
                timerThread.interrupt();
                changeSoal();
            }
        };
        View.OnClickListener salah = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                timerThread.interrupt();
                changeSoal();
            }
        };
        btA.setOnClickListener(benar);
        btB.setOnClickListener(salah);
        btC.setOnClickListener(salah);
        btD.setOnClickListener(salah);

        /*Timer waktu*/
        if (timerThread == null || timerThread.getState() == Thread.State.TERMINATED) {
            Runnable runnable = new Runnable() {
                int timer = 20;
                @Override
                public void run() {
                    try {
                        while(timer > 0) {
                            Thread.sleep(1000);
                            txtTimer.post(new Runnable() {
                                @Override
                                public void run() {
                                    String txt = "Waktu tersisa : " + timer;
                                    txtTimer.setText(txt);
                                    timer--;
                                }
                            });
                        }
                        timerThread.interrupt();
                        changeSoal();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            };
            timerThread = new Thread(runnable);
            timerThread.start();
        }
        /*-----------------*/
    }

    private void changeSoal() {
        FragmentManager fm;
        FragmentTransaction ft;
        fm = getActivity().getSupportFragmentManager();
        ft = fm.beginTransaction();
        Soal3Indonesia soal3Indo = new Soal3Indonesia();
        ft.replace(R.id.soalContainer, soal3Indo);
        ft.commit();
    }

    private void changeIntent(){
        Intent inten2 = new Intent(getActivity(),TotalSkor.class);
        startActivity(inten2);
    }

    /*-----------------------------------------------------------------*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_soal2_indonesia, container, false);
    }

}
