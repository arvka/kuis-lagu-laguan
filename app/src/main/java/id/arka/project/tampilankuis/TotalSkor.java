package id.arka.project.tampilankuis;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class TotalSkor extends AppCompatActivity {

    TextView tSkor,tNama;
    Button btnShare,btnKembali;
    private NotificationManager notifyManager;
    private NotificationCompat.Builder notifyBuilder;
    private Notification notifSkor;

    private static final int NOTIFICATION_ID = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_total_skor);

        SharedPreferences name = getSharedPreferences("NamaPreference", MODE_PRIVATE);
        String namaUser = name.getString("nama","");
        String displayNama = "";
        if(namaUser != null){
            displayNama = namaUser;
        }
        final int totalSkor = Kuis.skor;
        String display = "Halo, " + namaUser;
        tNama = (TextView) findViewById(R.id.namaUser);
        tSkor = (TextView) findViewById(R.id.skorTotal);
        tSkor.setText(String.valueOf(totalSkor));
        tNama.setText(display);
        final String finalDisplayNama = displayNama;
        View.OnClickListener share = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri uri = Uri.parse("smsto:");
                Intent it = new Intent(Intent.ACTION_SENDTO, uri);
                it.putExtra("sms_body", "Halo semuaa !! saya " + finalDisplayNama + " mendapatkan skor : " + totalSkor);
                startActivity(it);
            }
        };
        View.OnClickListener kembali = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intens = new Intent(TotalSkor.this,MainMenu.class);
                notifikasi(totalSkor);
                Kuis.skor = 0;
                startActivity(intens);
            }
        };
        btnShare = (Button) findViewById(R.id.btnShare);
        btnKembali = (Button) findViewById(R.id.btnKembali);
        btnShare.setOnClickListener(share);
        btnKembali.setOnClickListener(kembali);
    }

    public void notifikasi(int totalSkor){
        notifyManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notifyBuilder = new NotificationCompat.Builder(this).setContentTitle("Kerja yang Bagus !!").setContentText("Skormu adalah : " + totalSkor).setSmallIcon(R.drawable.logomusic);
        notifSkor = notifyBuilder.build();
        notifyManager.notify(1,notifSkor);
    }
}
