package id.arka.project.tampilankuis;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by HP on 14/12/2017.
 */

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.customViewHolder> {
    LayoutInflater mInflater;
    ArrayList<menuItem> menu;
    Context ctx;

    public CustomAdapter(Context context,ArrayList<menuItem> menu){
        this.mInflater = LayoutInflater.from(context);
        this.ctx = context;
        this.menu = menu;
    }

    @Override
    public CustomAdapter.customViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate( R.layout.menurow, parent, false);
        return new customViewHolder(itemView, this);
    }

    @Override
    public void onBindViewHolder(CustomAdapter.customViewHolder holder, int position) {
        final menuItem current = menu.get(position);
        holder.namaItemView.setText(current.nama);
        holder.gambarLogo.setImageResource(current.logo);
        holder.namaItemView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                startKuis(current,view);
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.menu.size();
    }

    public void startKuis(menuItem op,View v){
        if(op.nama == "Set Profil Pemain"){
            Intent inten = new Intent(v.getContext(),SetNama.class);
            ctx.startActivity(inten);
        }else{
            Intent inten = new Intent(v.getContext(),SoalTradisional.class);
            inten.putExtra("judul",op.nama);
            ctx.startActivity(inten);
        }
    }

    class customViewHolder extends RecyclerView.ViewHolder{
        TextView namaItemView;
        ImageView gambarLogo;

        public customViewHolder(View itemView, CustomAdapter adapter) {
            super(itemView);
            namaItemView = itemView.findViewById(R.id.menuitem);
            gambarLogo = itemView.findViewById(R.id.logo);
        }
    }
}
