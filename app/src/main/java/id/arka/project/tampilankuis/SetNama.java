package id.arka.project.tampilankuis;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SetNama extends AppCompatActivity {

    Button btNama,btKembali;
    EditText txtNama;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_set_nama);

        txtNama = (EditText) findViewById(R.id.txtNama);
        btNama = (Button) findViewById(R.id.btnNama);

        btNama.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences pref = getApplicationContext().getSharedPreferences("NamaPreference", MODE_PRIVATE);
                SharedPreferences.Editor editor = pref.edit();
                editor.putString("nama",txtNama.getText().toString());
                editor.commit();
                Toast.makeText(getApplicationContext(),"Nama berhasil disimpan",Toast.LENGTH_SHORT).show();
                Intent inten = new Intent(getApplicationContext(),MainMenu.class);
                startActivity(inten);
            }
        });
    }
}
