package id.arka.project.tampilankuis;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class SoalTradisional extends AppCompatActivity {

    private FragmentManager fm;
    private FragmentTransaction ft;
    TextView txtJudul;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_soal_tradisional);
        Bundle extras = getIntent().getExtras();
        txtJudul = (TextView) findViewById(R.id.txtJudul);
        String data = "";
        if(extras != null){
            data = extras.getString("judul");
        }
        String posisi = Kuis.soal + " / 5";
        txtJudul.setText(data);

        fm = getSupportFragmentManager();
        ft = fm.beginTransaction();
        if(data.equals("Lagu Indonesia")){
            Soal1Indonesia soal1 = new Soal1Indonesia();
            ft.replace(R.id.soalContainer,soal1);
        }else if(data.equals("Lagu Tradisional")){
            Soal1Tradisional soal1 = new Soal1Tradisional();
            ft.replace(R.id.soalContainer,soal1);
        }else{
            Soal1Mancanegara soal1 = new Soal1Mancanegara();
            ft.replace(R.id.soalContainer,soal1);
        }
        ft.commit();
    }
}
