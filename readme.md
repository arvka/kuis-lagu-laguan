﻿
# KUIS LAGU-LAGUAN

Kuis lagu-laguan is simple android application about song guessing quiz game. This application runs great on smartphone with HD resolution (720x1024).

## Important Note

This program is not completely working source code nor ready to run so you only download the project, create your project, copy and modify java file and resource file in your project. For complete folder information, see the project structure below.

## Folder Information

```
Project   
│
└───.idea
│	│
│	└───copyright
│   
└───app
│	│
│	└───build
│	│	│
│	│	└───outputs
│	│		│
│	│		└───apk //application debug apk can be found here
│	│		│
│	│		└───logs
│	│
│	└───src
│		│
│		└─── androidTest/java/id/arka/project/tampilankuis
│		│
│		└───main
│		│	│
│		│	└───java/id/arka/project/tampilankuis
│		│	│
│		│	└───res
│		│		│
│		│		└───drawable //image and icons used for this project
│		│		│
│		│		└───layout //application layout
│		│		│
│		│		└───values //custom values
│		│		│
│		│		└───....
│		│
│		└─── test/java/id/arka/project/tampilankuis
│   
└───build
│	│
│	└───android-profile
│	│
│	└───generated
│	│
│	└───intermediates/dex-cache
│   
└───gradle
	│
	└───wrapper	
```

## Authors

* **Arka Fadila Yasa**
* **Dhany Sakti Priabudi**
* **Ibnu Najah Barlian Hadi Utomo**
* **Rakha Fakhruddin**

## License

See in license page and of course each member of this team have driving license.

## Thanks to 

 - Allah, our almighty god. So we can complete this project.
 - Our parents.
 - Mobile application programming course lecture.
 - My lov... eh wait, i'm still single.
 - Random person who read this readme file, congratulations !! yout are the one of the rarest person in the Round Earth.

